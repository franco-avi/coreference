name := "Disambiguation"

version := "1.0"

libraryDependencies += "org.apache.spark" %% "spark-core" % "0.9.1"

libraryDependencies += "org.apache.spark" %% "spark-graphx" % "0.9.1"

resolvers += "Akka Repository" at "http://repo.akka.io/releases/"