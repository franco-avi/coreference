// Auhtor: Franco Avi

import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._

/*	
	Singleton object for reading and loading graphs file.
	The text files are in this format:
	id \t label \t [doc0, doc1, ..., docN] \t (neighbour1id + 1) \t (neighbour2id + 1) \t ... \t (neighbourKid + 1)
	
	Label of amibguos words are in this shape:
	ambiguos_word:docId
*/
object GraphBuilder {

	// Create the graph form the specified source file
	def create(filePath: String, context: SparkContext, numColors: Int) = {
		
		val vertexRdd = context.textFile(filePath).map(line => vertexBuilder(line, numColors))
		val edgeRdd = context.textFile(filePath).flatMap(line => edgeBuilder(line))

		println("vertices: " + vertexRdd.count + " edges: " + (edgeRdd.count / 2).toString)
		Graph.apply(vertexRdd, edgeRdd)
	}

	// Transform a line into a vertex
	private def vertexBuilder(line: String, numColors: Int) = {
		val splitted = line.split("\t")
	
		val id = splitted(0).trim.toLong
		val label = splitted(1)
		val docs = splitted(2).substring(1, splitted(2).length - 1).split(",").map(el => el.trim.toInt)
		
		// Only non-ambiguos nodes receive an initial coloring based on documents in which their label is contained 
		val colors = Array.fill(numColors){ 0.0f }
		
		if (!label.contains(":")) { 
			for (doc <- docs) colors(doc) = 100000.0f
		}
		
		(id, (label, colors))
	}	

	// Transform a line into an array of edges. Use this function with flatMap
	private def edgeBuilder(line: String) = {
		val splitted = line.split("\t")
		val id = splitted(0).trim.toLong
	
		splitted.drop(3).map(el => new Edge(id, el.trim.toLong - 1L, 1.0f))
	} 		
}
