// Auhtor: Franco Avi

import org.apache.spark.graphx
import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import org.apache.spark.storage._
import org.apache.log4j._

// Singleton object which contains the core functions of the algorithm
object Disambiguation {

// Compute the sum of two arrays of Float for reduce operations. WARN: you have to allocate a new array!
private def sumArrays(a: Array[Float], b: Array[Float]) = {
	val result = new Array[Float](a.length)
	
	var i = 0
	for (i <- 0 to result.length - 1) result(i) = a(i) + b(i)
		
	result
}

// Compute the sum of two arrays of Int for reduce operations. WARN: you have to allocate a new array!
private def sumArrays(a: Array[Int], b: Array[Int]) = {
	val result = new Array[Int](a.length)
	
	var i = 0
	for (i <- 0 to result.length - 1) result(i) = a(i) + b(i)
	
	result
}

// Return the index of the max value in the array. An init index can be provided
private def maxIndex(colors: Array[Float], init: Int = 0) = {
	var maxIndex = init
	var i = 0
	for (i <- 0 to colors.length - 1) if (colors(i) > colors(maxIndex)) maxIndex = i
	
	maxIndex
}

/* 
	Compute most popular color for every node in the graph. An rdd in the shape (VertexId, MostPopularColor) is returned.
	The most popular color for a node v is the one that has the maximum volume in the neighborhood of v and in v itself.
	If two or more colors have the same volume, the color matching the color tag is picked.
*/
private def computeMostPopular(g: Graph[Array[Float], Float], colorTags: RDD[(VertexId, Int)]) = {
	// Compute colors' volumes in the neighborhood
	val volume = g.mapReduceTriplets(et => Iterator( (et.dstId, et.srcAttr) ), (a: Array[Float], b: Array[Float]) => sumArrays(a,b) )
	
	val result = volume.join(g.vertices)
					// Sum the colors of the node with the ones of its neighborhood
					.map{ case (id, (vol, c)) => (id, sumArrays(c, vol)) } 
					.join(colorTags)
					// Choose the maximum index: the index of the color tag has precedence
					.map{ case (id, (c, colorTag)) => (id, maxIndex(c, colorTag)) }
	
	result
}

/*
	 Compute the population array for each vertex in the graph.
	 population(i) of vertex v contains the number of neighbors of v that have color tag equal to i.
*/
private def computePopulation(g: Graph[Array[Float], Float], colorTags: RDD[(VertexId, Int)]) = {
	// This function return an array fill of 0s except for the element of index equal to the color tag (with value of 1).
	// In this way it's possible to reduce the result
	def f(et: EdgeTriplet[(Array[Float], Int), Float]) = { val result = Array.fill(et.srcAttr._1.length){ 0 }; result(et.srcAttr._2) = 1; result }
	
	var pop = g.outerJoinVertices(colorTags){ case (id, old, colorTag) => (old, colorTag.get) }
				.mapReduceTriplets(et => Iterator( (et.dstId, f(et)) ), (a: Array[Int], b: Array[Int]) => sumArrays(a,b) )
	
	pop
}

/*
	Diffuse the color (boundary force in the article). This function is applied to every node in the graph.
	A fraction alpha of most popular color (alpha = 1 / 10000) is send to all neighbors.
	Other colors are sent entirely to all neighbors (round <= 30) or to the neighbors with the same color tag (round > 30).
*/
private def diffuse(colors: Array[Float], mostPop: Int, pop: Array[Int], i: Int, dstColorTag: Int, edgeW: Float) = {
	var result: Array[Float] = null
	
	if (i <= 30){
		// edgeW is = 1.0 / degree
		result = colors.map(c => c * edgeW)
	} else {
		result = Array.fill(colors.length){ 0.0f }
		result(dstColorTag) = colors(dstColorTag) / pop(dstColorTag)
	}
	
	result(mostPop) = colors(mostPop) * (edgeW / 10000.0f)
	
	result
}

// Add a share quantity to the element of colors array specified by the tag. WARN: you have to allocate a new array!
private def refillColorTag(colors: Array[Float], tag: Int, share: Float) = {
	val result = colors.clone
	
	result(tag) += share
	
	result
}

// Run the algorithm. An rdd of tuples (graphVertexId, clusterId) is returned
def run(graph: Graph[Array[Float], Float], initColorTags: RDD[(VertexId, Int)], numColors: Int, sc: SparkContext) = {

	val degrees = graph.outDegrees.cache
	
	// Add a weight to each edge equal to 1.0 / degree
	var g = graph.outerJoinVertices(degrees){ case (id, old, deg) => (old, deg.getOrElse(0)) }
				.mapTriplets(et => 1.0f / et.srcAttr._2.toFloat)
				.mapVertices{ case (id, (old, deg)) => old }

	var colorTags = initColorTags
	
	var i = 0
	var t = System.currentTimeMillis()		
	while (i < 30) {
	
		// Compute rdd of (VertexId, MostPopularId)
		var mostPop = computeMostPopular(g, colorTags)
		// Compute rdd of (VertexId, Poulation[])
		var pop		= computePopulation(g, colorTags)
		
		// Merge all previously computed infos in a single rdd for simpler usage		
		var popInfo = mostPop.join(pop).join(colorTags).map{ case (id, ((mostPop, pop), colorTag)) => (id, (mostPop, pop, colorTag)) }
		
		// Compute the inbound flow for every node in the graph
		val flow = g.outerJoinVertices(popInfo){ case (id, c, popInfo) => (c, popInfo.get._1, popInfo.get._2, popInfo.get._3) }		
					.mapReduceTriplets( et => Iterator((et.dstId, diffuse(et.srcAttr._1, et.srcAttr._2, et.srcAttr._3, i, et.dstAttr._4, et.attr) )),
									  ( a: Array[Float], b: Array[Float]) => sumArrays(a,b) )
		
		// Erase all colors' volumes that have been sent in the previous step
		g = g.outerJoinVertices(mostPop){ case (id, c, mostPop) => (c, mostPop.getOrElse(0)) }
			 .mapVertices{ case (id, (colors, mostPop)) => colors.zipWithIndex.map{ case (c, i) => if (i == mostPop) (c - c / 10000.0f) else 0.0f } } 
	
		// Add the inbound flow to the actual nodes of the graph (flush operation)
		g = g.outerJoinVertices(flow){ case (id, c, flow) => sumArrays(c, flow.getOrElse(Array.fill(c.length){ 0.0f })) }
		
		// Compute updated color tags. Old color tags are used for initialization
		colorTags = g.vertices.join(colorTags).map{ case (id, (colors, oldColorTag)) => (id, maxIndex(colors, oldColorTag)) }
		
		// Round all colors of each node in the graph in order to avoid approximation problems
		g = g.mapVertices{ case (id, colors) => colors.map(c => c.round.toFloat) }
		
		// Compute rdd of (VertexId, MostPopularId)
		mostPop = computeMostPopular(g, colorTags)
		// Compute rdd of (VertexId, Poulation[])
		pop		= computePopulation(g, colorTags)				
		
		// Compute the set of interior nodes. An interior nodes is a node that satisfy this condition: population(most_popular) == degree
		// An rdd of this shape (VertexId, MostPopularId) of ONLY interior nodes is returned
		val interior = mostPop.join(pop).join(degrees).filter{ case (id, ((mostPop, pop), deg) ) => pop(mostPop) == deg }.map{ case (id, ((mostPop, pop), deg) ) => (id, mostPop) }
		
		// Every interior node puts in the inventory array all colors' volumes except for the most popular one
		val inventory = if (interior.count > 0)
						g.vertices.join(interior)
						 .map{ case (id, (colors, mostPop)) => colors.zipWithIndex.map{ case (c, i) => if (i == mostPop) 0.0f else c } }
						 .reduce( (a: Array[Float], b: Array[Float]) => sumArrays(a,b) )
						else
						Array.fill(numColors){ 0.0f }
						
		// Erase all colors' volumes that have been sent in the previous step
		g = g.outerJoinVertices(interior){ case (id, colors, mostPop) => if (mostPop.isEmpty) colors else colors.zipWithIndex.map{ case (c, i) => if (i == mostPop.get) c else 0.0f } }
		
		// Calculate the population of the entire graph.
		// Distribution(i) contains the number of nodes with color tag = i
		val distribution = colorTags.map{ case (id, tag) => (tag, 1) }
							.union( sc.parallelize((0 to numColors - 1).toArray.map(n => (n, 0))) )
							.reduceByKey((a: Int, b:Int) => a + b)
							.takeOrdered(numColors)
							.map{ case (k, v) => v }
		
		// Share inventory with all nodes. Each node increase it's color-tag's volume by a quantity = inventory(color-tag) / distribution(color-tag)
		g = g.outerJoinVertices(colorTags){ case (id, old, tag) => (old, tag) }
			.mapVertices{ case (id, (c, tag)) => refillColorTag(c, tag.get, if (distribution(tag.get) > 1) inventory(tag.get) / distribution(tag.get) else 0.0f) }.cache
			
		// Materialize g after the caching of the previous line
		g.vertices.count
		
		println("Ended round: " + i + " time: " + (System.currentTimeMillis() - t)/1000 )
		t = System.currentTimeMillis()
		i += 1
		

	}
	
	// Uncache outDegrees
	degrees.unpersist()
		
	// Return rdd of color tags
	colorTags
	}
	
	// Measure and print precision, recall and F1 score for the computed clusters, given a ground truth file
	def measureAccuracy(groundTruthPath: String, sc: SparkContext, clusters: RDD[(Int, Seq[Int])]) = {
		
		// Return an integer equal to the number of values that are in a and are also in b
		def union(a: Array[Int], b: Array[Int]) = {
			var count = 0
			for (idA <- a) if (b.contains(idA)) count += 1
			
			count
		}
	
		// Read and init the ground truth. The rdd returned is in this shape: (idDoc, clusterDocsList)
		val gt = sc.textFile(groundTruthPath)
					.flatMap{ line => 
						val gtCluster = line.split("\t").map(s => s.trim.toInt)
						val elements = gtCluster.map(id => (id, gtCluster))
								
						elements
					}
		
		// Init the computed clusters. The rdd returned is in this shape: (idDoc, clusterDocsList)
		val c = clusters.flatMap{ case (id, list) => list.map(id => (id, list.toArray)) }	
		
		// Compute total precision and recall
		val p = sc.accumulator(0.0)
		val r = sc.accumulator(0.0)
		c.join(gt).foreach{ case (id, (list1, list2)) =>
						val count = union(list1, list2)
						
						p += 100.0 * count / list1.length
						r += 100.0 * count / list2.length
					}
		
		// Average the results with the total number of documents
		val precision = p.value / c.count
		val recall = r.value / c.count
		val F1 = 2 * (precision * recall) / (precision + recall)
		
		println("precision: " + precision + "\trecall: " + recall + "\tF1: " + F1)
	}
}
		