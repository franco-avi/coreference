// Auhtor: Franco Avi

import org.apache.spark._
import org.apache.spark.rdd.PairRDDFunctions
import org.apache.spark.graphx
import org.apache.spark.graphx._
import org.apache.log4j.PropertyConfigurator
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._


object App {
  
  def main(args: Array[String]) {
  
  	// Configure spark and log4j 
 	PropertyConfigurator.configure("log4j.properties")
 	Utils.clustInit
    
	val sc = Utils.Context
    
    val graphFile = Utils.getFileName("data/CBZ9z_filtered1.graph")//"data/xmljs1.graph"
    val groundTruthFile = Utils.getFileName("data/ground-truth-CBZ9z.txt") //"data/ground-truth-xmljs.txt""
    val numDocs = 1185//97  
	
	// Create the graph: each node contains a label and an array of colors
	val g = GraphBuilder.create(graphFile, sc, numDocs)
	
	// docsV contains tuples in the shape (graphVertexId, docId) of ambiguos nodes only
	val docsV = g.vertices.filter{ case (id, (label, v)) => label.contains(":") }
    			.map{ case (id, (label, v)) => (id.toLong, label.split(":")(1).trim.toInt) }
    			
    // Run the algorithm. An rdd of tuples (graphVertexId, clusterId) is returned
	val colorTags = Disambiguation.run(
						g.mapVertices{ case (id, (label, colors)) => colors },
						// If a node is ambiguos then the initial color tag is the one of its document, otherwise it's 0 
						g.vertices.map{ case (id, (label, colors)) => if (label.contains(":")) (id, label.split(":")(1).trim.toInt) else (id, 0) }, 
						numDocs, sc)
    
    // Print the results 
    val result = docsV.join(colorTags)
		 .map{ case (id, (doc, colorTag)) => (colorTag, doc) }
		 .groupByKey() 
		 
	result.map{ case (id, list) => list.sorted.mkString(" ") + "\n------------------------------------------------------" }
		 .foreach(println)
	
	// Print precision, recall and F1 score
	Disambiguation.measureAccuracy(groundTruthFile, sc, result)		  
  }
  
}
