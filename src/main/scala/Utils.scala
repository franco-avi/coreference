
import scala.io.Source
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object Utils {
  
  private var context: Option[SparkContext] = None


  def clustInit() = {
    val sparkHome = "/root/spark/"
    val jarFile = "target/scala-2.10/disambiguation_2.10-1.0.jar"
    val master = Source.fromFile("/root/spark-ec2/cluster-url").mkString.trim
    val masterHostname = Source.fromFile("/root/spark-ec2/masters").mkString.trim

    val conf = new SparkConf()
             .setMaster(master)
             .setAppName("Disambiguation")
             .setJars(Seq(jarFile))
	     .setSparkHome(sparkHome)
             .set("spark.executor.memory", "2560m")
    context = Some(new SparkContext(conf))
  }
  
  def localInit() = {
    context = Some(new SparkContext("local[4]","Disambiguation","/usr/local/Cellar/amplab-graphx-f854498"))
  }
  
  def Context():SparkContext = {
    if (context.isEmpty )
      localInit()
    return context.get
  }
  
  def getFileName(filename:String) : String = {
    if (Context.isLocal)
      return filename
    val masterHostname = Source.fromFile("/root/spark-ec2/masters").mkString.trim
    val nfile = s"hdfs://$masterHostname:9000/$filename"
    return nfile
  }
  
}